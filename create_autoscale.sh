#!/bin/bash
Name="Edges_Group"
cmd_num=0
dply_num=0

create_autoscale_func (){
    aws autoscaling create-auto-scaling-group --auto-scaling-group-name "Edges_Group0-$1" \
    --launch-configuration-name CodeDeployEdgesCopyCopy \
    --min-size 0 \
    --max-size 30000 \
    --desired-capacity 0 \
    --vpc-zone-identifier "subnet-615b872c,subnet-b40901e8,subnet-433b3724,subnet-19686237,subnet-7e7b2840,subnet-58985956" \
    --tags "Key=Name,Value=Noop Jarivs Edge" "Key=command,Value=0" "Key=deploy,Value=$1" "Key=type,Value=edge"

    echo "Create group$1"
}


aws autoscaling create-auto-scaling-group --auto-scaling-group-name "Edges_Group0" \
--launch-configuration-name CodeDeployEdgesCopyCopy \
--min-size 0 \
--max-size 30000 \
--desired-capacity 0 \
--vpc-zone-identifier "subnet-615b872c,subnet-b40901e8,subnet-433b3724,subnet-19686237,subnet-7e7b2840,subnet-58985956" \
--tags "Key=Name,Value=Noop Jarivs Edge" "Key=command,Value=0" "Key=deploy,Value=0" "Key=type,Value=edge"

echo "Create group0"

for i in $(seq 1 63); do
    create_autoscale_func $i
done
