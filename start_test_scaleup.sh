#!/bin/bash

start=1
max=24
desired_capa=4

set_autoscale_func (){
    aws autoscaling set-desired-capacity --auto-scaling-group-name $1 \
    --desired-capacity $2
    echo "Start group$3 as $2"
}

set_autoscale_func "Edges_Group0" $desired_capa 0

for i in $(seq 1 $max); do
    set_autoscale_func "Edges_Group0-$i" $desired_capa $i
    #set_autoscale_func "Edges_Group$i" $desired_capa $i
done

current_edge=0
total_edge=$(($desired_capa * ($max+1)))
while [ $current_edge != $total_edge ]; do
    echo "Sleep 2 seconds..."
    sleep 2s
    str=$(aws ec2 describe-instances --filters "Name=instance-state-code,Values=16" "Name=tag:type,Values=edge" --query 'Reservations[].Instances[].[PrivateIpAddress,InstanceId,Tags[?Key==`Name`].Value[]]' --output text | sed 's/None$/None\n/' | sed '$!N;s/\n/ /' | awk '{print $2;}')
    instanceId=(
        ${str}
    )
    current_edge=${#instanceId[@]}
    echo "Current edges: $current_edge"
done

#./deploy_test_scaleup.sh
./deploy_single_test_scaleup.sh
echo "done..."
