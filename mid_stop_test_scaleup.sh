#!/bin/bash

max=24
start=$(($max-$1+1))
setas0=0

for i in $(seq $start $max); do
    aws autoscaling set-desired-capacity --auto-scaling-group-name Edges_Group0-$i \
    --desired-capacity $setas0
    echo "Stop group$i as $setas0"
done

echo "done..."
 