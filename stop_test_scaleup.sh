#!/bin/bash

start=1
max=24
setas0=0

aws autoscaling set-desired-capacity --auto-scaling-group-name Edges_Group0 \
--desired-capacity $setas0
echo "Stop group0 as $setas0"

for i in $(seq 1 $max); do
    #aws autoscaling set-desired-capacity --auto-scaling-group-name Edges_Group$i \
    aws autoscaling set-desired-capacity --auto-scaling-group-name Edges_Group0-$i \
    --desired-capacity $setas0
    echo "Stop group$i as $setas0"
done

echo "done..."
