#!/bin/bash

for i in $(seq 32 63); do
    aws deploy create-deployment-group \
    --application-name MiNiFi \
    --deployment-group-name "Scale$i" \
    --deployment-config-name "CodeDeployDefault.OneAtATime" \
    --ec2-tag-filters "Key=deploy,Value=$i,Type=KEY_AND_VALUE" \
    --service-role-arn "arn:aws:iam::758947377502:role/CodeDeployRole"
    echo "Create deployment group$1"
done
