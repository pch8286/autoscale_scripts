#!/bin/bash

deploy_func() {
    aws deploy create-deployment --application-name MiNiFi --deployment-config-name CodeDeployDefault.AllAtOnce --deployment-group-name Scale$1 --description "MiNiFi deploy Scale$1" --github-location repository="chopark/CodeDeploy_MiNiFi",commitId=$2
}

max=24
commit_id="b2da0becff9f8577de0dafce05449da4cd4b81b3"

for i in $(seq 0 $max); do
    deploy_func $i ${commit_id}
    echo "Deploy group$i"
done
