#!/bin/bash
max=$(($1-1))

sed -i "s~max=.*~max=$max~g" *_test_scaleup.sh
sed -i "s~desired_capa=.*~desired_capa=$2~g" *_test_scaleup.sh
