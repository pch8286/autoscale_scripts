#!/bin/bash

start=1
max=63
setas1=1

set_autoscale_func (){
    aws autoscaling set-desired-capacity --auto-scaling-group-name $1 \
    --desired-capacity $2
    echo "Start group$3 as $2"
}

set_autoscale_func "Edges_Group0" $setas1 0


for i in $(seq 1 $max); do
    set_autoscale_func "Edges_Group0-$i" $setas1 $i
done

echo "done..."
