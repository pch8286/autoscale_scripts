# Table of Contents
- [Table of Contents](#table-of-contents)
- [Prerequisite](#prerequisite)
- [Scripts](#scripts)
  - [create_autoscale.sh](#create_autoscalesh)
  - [create_deploy_group.sh](#create_deploy_groupsh)
  - [set_max.sh](#set_maxsh)
    - [Example](#example)
  - [start_test_scaleup.sh](#start_test_scaleupsh)
    - [Example](#example-1)
  - [stop_test_scaleup.sh](#stop_test_scaleupsh)
    - [Example](#example-2)
  - [mid_stop_test_scaleup.sh](#mid_stop_test_scaleupsh)
    - [Example](#example-3)
  - [deploy_test_scaleup.sh](#deploy_test_scaleupsh)
    - [Example](#example-4)

# Prerequisite
* CodeDeploy GitHub setup
* Install and configure AWS cli

# Scripts

## create_autoscale.sh
* create autoscale groups for port related deployment
* **Need to change vpc-zone-identifier**


## create_deploy_group.sh
* create CodeDeploy groups for port related deployment
* **Need to change vpc-zone-identifier**
* **Need to change the number of groups manually**

## set_max.sh
* Setup how many edges to start
* The group used for edges / per ports (i.e the number of ports)

### Example
```console
$ bash set_max.sh (the number of groups) (the number of edges)

// 6 groups 4 edges (total 24 edges, 4 edges / ports)
$ bash set_max.sh 6 4 
```

## start_test_scaleup.sh
* Start the edges and wait for edges starting up
* Then, automatically deploy minifi to edges with `deploy_test_scaleup.sh`
* **Need to change commit ids from CodeDeployMiNiFi in deploy_test_scaleup.sh**  if you changed MiNiFi in GitHub

### Example
```console
$ bash start_test_scaleup.sh
Start group0 as 4
Start group1 as 4
        (...)
Sleep 2 seconds...
Current edges: 0
Sleep 2 seconds...
Current edges: 7
Sleep 2 seconds...
Current edges: 12
        (...)
Sleep 2 seconds...
Current edges: 24

//Deployment text
done...
```

## stop_test_scaleup.sh
* Stop the groups and edges

### Example
```console
$ bash stop_test_scaleup.sh
```

## mid_stop_test_scaleup.sh
* Stop the groups from back
* **Need to stop NiFi ports in GUI**

### Example
```console
$ bash mid_stop_test_scaleup.sh (the number of groups)

// If you have Scale Group Id 1 to 5, it will turn off(set to 0) instances of Scale Group Id 5.
$ bash mid_stop_test_scaleup.sh 1
// If you have Scale Group 1 to 5, it will turn off(set to 0) instances of Scale Group Id 4, 5.
$ bash mid_stop_test_scaleup.sh 2
```

## deploy_test_scaleup.sh
* deploy minifi to the edges
* Do not run again if the deployment process was successful in start_test_scaleup script.
* **Need to change commit ids from CodeDeployMiNiFi** if you changed MiNiFi in GitHub

### Example
```console
$ bash deploy_test_scaleup.sh
```
